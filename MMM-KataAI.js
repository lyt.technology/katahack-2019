Module.register("MMM-KataAI", {
  defaults: {
    welcomeMessage: "Katakan 'Halo' untuk memulai percakapan.",
    google: {
      credentialPath: "credentials.json", // credentials.json
      languageCode: "id-ID", // id-ID, en-US
     
    },
    record: {
      recordProgram: "arecord",
      sampleRate: 16000,
      channels: 1,
      threshold: 0.5,
      silence: '5.0',
    },
    kata: {
      webhookURL: ""
    }
  },

  start: function(){},

  getDom: function(){
    var wrapper = document.createElement("div");
    wrapper.className = "reply";
    wrapper.innerHTML = this.config.welcomeMessage;
    return wrapper;
  },

  getStyles: function(){
    return ["MMM-KataAI.css"];
  },

  socketNotificationReceived: function(notification, payload) {
  }
});
