# MMM-KataAI
Katahack 2019.
The purpose of this module is to allow communication between Bot made in Kata Platform with user using our pre-made Smart Mirror.

## Dependencies
In order for this module to work we need
* [MagicMirror](https://github.com/MichMich/MagicMirror)

## Module Installation
Go to `MagicMirror/modules` directory and then:
```bash
  git clone https://gitlab.com/lyt.technology/katahack-2019.git MMM-KataAI
  cd MMM-KataAI
  npm install
```
