async function main() {
  const speech = require('@google-cloud/speech')
  const fs = require('fs')

  const client = new speech.SpeechClient()

  const fileName = './sound-sample-2.wav'

  const file = fs.readFileSync(fileName)
  const audioBytes = file.toString('base64')

  const audio = {
    content: audioBytes,
  }

  const config = {
    encoding: 'LINEAR16',
    audioChannelCount: 2,
    languageCode: 'id-ID',
    enableAutomaticPunctuation: true,
  }

  const request = {
    audio: audio,
    config: config
  }

  const [response] = await client.recognize(request)
  const res = response.results
    .map(result => result.alternatives[0].transcript)
    .join('\n')
  //console.log(`Transcription: ${res.transcription}`)
  console.log(`Confidence: ${res}`)
}
main().catch(console.error)
